<?php

// Create a new instance of the SoapClient class.
$params = array('trace' => 1, 'soap_version' => SOAP_1_1);
$BHclient = new SoapClient("https://api.bullhornstaffing.com/webservices-1.1/?wsdl",$params);

// To run this code, you will need a valid username, password, and API key.
$username = "Ntelicor.api";
$password = "490lisbon";
$apiKey = "C2EB8E6E-E164-B256-C80139FC60B82EEE";

//old password 490lisbon

// Start a new session
$session_request = new stdClass();
$session_request->username = $username;
$session_request->password = $password;
$session_request->apiKey = $apiKey;
$API_session = $BHclient->startSession($session_request);
$API_currentSession = $API_session->return;

// Create an array with the query parameters
$query_array = array(
  'entityName' => 'JobOrder',
  'maxResults' => 1000,
  //'where' => 'isDeleted=0',
  //'where' => 'isOpen=1',
  //'where' => "customText12 = 'Yes'",
  //'where' => "customText12 = 'Yes' AND dateAdded >= '07/01/2015'",
  'where' => "customText12 = 'Yes' AND isOpen=1 AND isDeleted=0",
  'orderBys' => array('dateAdded desc'),
  'parameters' => array()
);

// Create the DTO type that the API will understand by casting the array to the dtoQuery
// type that the query operation expects.
$SOAP_query = new SoapVar($query_array, SOAP_ENC_OBJECT,"dtoQuery", "http://query.apiservice.bullhorn.com/");

// Put the DTO into a request object
$request_array = array ('session' => $API_currentSession, 'query' => $SOAP_query);

// Cast the request as a query type
$SOAP_request = new SoapVar($request_array, SOAP_ENC_OBJECT, "query", "http://query.apiservice.bullhorn.com/");

// Use the query method to return the candidate ids
try {
  $queryResult = $BHclient->query($SOAP_request);
} catch (SoapFault $fault) {
  var_dump($BHclient->__getLastRequest());
  die($fault->faultstring);
}

?>

<table id="bullhorn-results">

  <?php

  // Use the find() method to retrieve the candidate DTO for each Id
  // Loop through each Id in the query result list
  $i = 0;
  foreach ($queryResult->return->ids as $value) {
    $i++;
    // Cast each Id to an integer type
    $findId = new SoapVar($value, XSD_INTEGER,"int","http://www.w3.org/2001/XMLSchema");

    // Create the find() method request
    $find_request = array(
      'session' => $API_currentSession,
      'entityName' => 'JobOrder',
      'id' => $findId
    );

    // Use the find() method to return the candidate dto
    try {
      $findResult = $BHclient->find($find_request);
    } catch (SoapFault $fault) {
      var_dump($BHclient->__getLastRequest());
      die($fault->faultstring);
    }
    //print_r($findResult->return->dto);
    // Print fields from the dto object

  ?>

    <tr>
      <td>
        <div class="title color-<?php echo($i%2); ?>">
          <span class="title-text"><?php echo $findResult->return->dto->title; ?></span>
          <div class="more">About this Position</div>
          <div class="clear"></div>
        </div>
        <div class="details">
          <span class="details-text"><?php echo strip_tags($findResult->return->dto->publicDescription, "<p><b><u><a><br><ul><i><ol><li>"); ?></span>
          <div class="clear"></div>
          <div class="submit-resume" data-contact="<?php echo $findResult->return->dto->responseUserID; ?>" data-job="<?php echo($value); ?>" data-title="<?php echo $findResult->return->dto->title; ?>">SUBMIT YOUR RESUME</div>
          <div class="clear"></div>
        </div>
      </td>
    </tr>

  <?php
  }
  ?>
</table>

<div id="resume-overlay"></div>
<div id="submit-resume">
  <div id="close-resume">close</div>
  <form id="submit-resume-form" action="#bullhorn-messages" method="post" enctype="multipart/form-data">
    <input type="hidden" value="" name="job_id" id="job-id-input" />
    <input type="hidden" value="" name="job_title" id="job-title-input" />
    <input type="hidden" value="" name="contact_id" id="contact-id-input" />
    <img src="/images/bullhorn/resume_header.png" />
    <div id="job-title">JOB TITLE: <span id="job-title-text"></span></div>
    <div class="text-field"><div>FIRST NAME</div><input class="required" type="text" name="first_name" /></div>
    <div class="text-field"><div>LAST NAME</div><input class="required" type="text" name="last_name" /></div>
    <div class="text-field"><div>EMAIL ADDRESS</div><input id="resume-email" class="required" type="text" name="email" /></div>
    <div class="text-field confirm"><div>CONFIRM EMAIL</div><input class="required confirm" type="text" name="confirm_email" /></div>
    <div class="text-field"><div>PHONE NUMBER</div><input class="required" type="text" name="phone" /></div>
    <div id="resume-file">ATTACH YOUR RESUME <span>(.DOC Files Only Please)</span><img id="choose-file" src="/images/bullhorn/file.png" alt="" /><div id="file-name"></div><input class="required" type="file" name="resume_file" /></div>
    <textarea class="js" default="COMMENTS (optional)" name="comments"></textarea>
    <input id="submit-form" type="submit" value="SUBMIT NOW" />
  </form>

</div>