<?php

  // If last results were fetched more than 10 minutes ago
  if( (time() - date("U", filemtime('search_results.html'))) > 600 ) {
    // Grab the search results table by running do_search.php and store it in search_results.html
    ob_start();
    include 'do_search.php';
    $results = ob_get_clean();
    file_put_contents('search_results.html', $results);
  } else {
    // Otherwise use the results from search_results.html
    $results = file_get_contents('search_results.html');
  }
  
  // Display the results
  //include('search_results.html');
  echo($results);

?>