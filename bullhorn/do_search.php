<?php

	// Create a new instance of the SoapClient class.
	$params = array('trace' => 1, 'soap_version' => SOAP_1_1);
	$BHclient = new SoapClient("https://api.bullhornstaffing.com/webservices-1.1/?wsdl",$params);

	// To run this code, you will need a valid username, password, and API key.
	$username = "Ntelicor.api";
	$password = "490lisbon";
	$apiKey = "C2EB8E6E-E164-B256-C80139FC60B82EEE";

	//old password 490lisbon

	// Start a new session
	$session_request = new stdClass();
	$session_request->username = $username;
	$session_request->password = $password;
	$session_request->apiKey = $apiKey;
	$API_session = $BHclient->startSession($session_request);
	$API_currentSession = $API_session->return;

	// Create an array with the query parameters
	$query_array = array(
		'entityName' => 'JobOrder',
		'maxResults' => 1000,
		//'where' => 'isDeleted=0',
		//'where' => 'isOpen=1',
		//'where' => "customText12 = 'Yes'",
		//'where' => "customText12 = 'Yes' AND dateAdded >= '07/01/2015'",
		'where' => "customText12 = 'Yes' AND isOpen=1 AND isDeleted=0",
		'orderBys' => array('dateAdded desc'),
		'parameters' => array()
	);

	// Create the DTO type that the API will understand by casting the array to the dtoQuery
	// type that the query operation expects.
	$SOAP_query = new SoapVar($query_array, SOAP_ENC_OBJECT,"dtoQuery", "http://query.apiservice.bullhorn.com/");

	// Put the DTO into a request object
	$request_array = array ('session' => $API_currentSession, 'query' => $SOAP_query);

	// Cast the request as a query type
	$SOAP_request = new SoapVar($request_array, SOAP_ENC_OBJECT, "query", "http://query.apiservice.bullhorn.com/");

	// Use the query method to return the candidate ids
	try {
		$queryResult = $BHclient->query($SOAP_request);
	} catch (SoapFault $fault) {
		var_dump($BHclient->__getLastRequest());
		die($fault->faultstring);
	}


  // Use the find() method to retrieve the candidate DTO for each Id
  // Loop through each Id in the query result list
  $i = 0;
  $data = array();

  foreach ($queryResult->return->ids as $value) {
    $i++;
    // Cast each Id to an integer type
    $findId = new SoapVar($value, XSD_INTEGER,"int","http://www.w3.org/2001/XMLSchema");

    // Create the find() method request
    $find_request = array(
      'session' => $API_currentSession,
      'entityName' => 'JobOrder',
      'id' => $findId
    );

    // Use the find() method to return the candidate dto
    try {
      $findResult = $BHclient->find($find_request);
    } catch (SoapFault $fault) {
      var_dump($BHclient->__getLastRequest());
      die($fault->faultstring);
    }

    $listing_object = $findResult->return->dto;
    $address = $listing_object->address;
	  $state = convertStateAbbrv($address->state);
	  $city = $address->city;
    $title = $findResult->return->dto->title;  // append city, state to job listing title
	  //$explode_title = explode('#',$title);
	  //$job_title_cleaned = substr($explode_title[1],5);
	  //$title = $job_title_cleaned;
    $desc = $findResult->return->dto->publicDescription;
    $contact = $findResult->return->dto->responseUserID;
    $job = $value;
	  $apply_link = "<a class='button submit-resume open-modal' data-contact='".$findResult->return->dto->responseUserID."' data-job='".$value."' data-title='".$findResult->return->dto->title."'>Submit Your Resume</a>";
	  $desc .= $apply_link;
	  $address->state = $state;
    $data['data'][] = array('title'=>$title,'desc'=>$desc, 'address'=>json_encode($address), 'city'=>$city, 'state'=>$state);

  }

  echo json_encode($data);

?>
