<?php

  // If last results were fetched more than 10 minutes ago
  if( (time() - date("U", filemtime('bullhorn/search_results.html'))) > 30 ) {
    // Grab the search results table by running do_search.php and store it in search_results.html
    ob_start();
    include 'bullhorn/do_search.php';
    $results = ob_get_clean();
    file_put_contents('bullhorn/search_results.html', $results);
  } else {
    // Otherwise use the results from search_results.html
    $results = file_get_contents('bullhorn/search_results.html');
  }

  // Display the results
  echo($results);

?>