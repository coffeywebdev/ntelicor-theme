<?php if(!$contact_found) { ?>
<h3>No Match for Job Contact Info ID!</h3>
Please contact HypeLife Brands to notify us what email address Job Orders assigned to contact ID <?php echo($_POST['contact_id']); ?> should be sent to.
<br />
<br />
<?php } ?>

<strong>Job Title: </strong> <?php echo($_POST['job_title']); ?><br />
<strong>Job ID: </strong> <?php echo($_POST['job_id']); ?><br />
<br />
<strong>Name: </strong> <?php echo($_POST['first_name'] . ' ' . $_POST['last_name']); ?><br />
<strong>Email: </strong> <a href="mailto:<?php echo($_POST['email']); ?>"><?php echo($_POST['email']); ?></a><br />
<strong>Phone: </strong> <?php echo($_POST['phone']); ?><br />
<strong>Resume: </strong> <a href="<?php echo($fileurl); ?>"><?php echo($basefile); ?></a><br />

<?php if(strlen($_POST['comments']) > 4) { ?>
  <strong>Comments: </strong>
  <br />
  <?php echo(stripslashes(trim($_POST['comments'])));
} ?>