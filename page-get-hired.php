<?php
  /* Template Name: Get Hired */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="listings-hero background-image background-black padding-TB-100" style="background-image: url(<?php echo $root; ?>/img/listings-hero-bg.jpg);">
  <!-- SHAPES -->
  <div class="left-dashed-hexagon"></div>
  <div class="left-red-hexagon"></div>
  <div class="right-dashed-hexagon"></div>
  <div class="right-red-hexagon"></div>
  <!-- END SHAPES -->
  <div class="container">
    <div class="row padding-TB-25">
      <div class="col-md-10 col-md-offset-1">
        <h1 class="text-huge text-with-subtitle no-margin fade-in">Get Hired<span class="text-red">.</span></h1>
        <p class="paragraph barlow-thin subtitle no-margin text-white fade-in">We turn IT Jobs Into IT Careers.</p>
      </div>
    </div>
  </div>
</div>
<div>
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h2 class="text-big text-black text-with-subtitle">
          For over 20 years, Ntelicor has been a leader in the information technology staffing industry<span class="text-red">.</span>
        </h2>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-8 col-md-offset-1">
        <p class="paragraph barlow barlow-normal text-black">
          In that time, we have successfully filled thousands of software developer jobs, network and security engineering jobs, BA, QA, automation and project manager jobs, and every other IT position in the books.
        </p>
        <p class="paragraph barlow barlow-normal text-black">
          We've developed this trust through providing the exact right fit for our clients. For selected candidates, this also means that you will only be recommended to companies that fit within your personal and professional growth trajectory.
        </p>
        <p class="paragraph barlow barlow-normal text-black">
          To see if you can benefit from our high-end clientele, fill out our application form below or <a title="Browse our open positions" href="/job-listings">browse our open positions</a> for your next IT job.
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <p class="align-center">
          <a href="/job-listings" title="Search Jobs" class="button">Search Jobs</a>
        </p>
      </div>
    </div>
  </div>
</div>
<div class="background-black background-image padding-TB-50" style="background-image: url(<?php echo $root; ?>/img/footer-cta-bg.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1 class="text-huge text-with-subtitle text-transform-none">
          We turn IT jobs into IT careers<span class="text-red">.</span>
        </h1>
        <p class="paragraph barlow text-white">
          Contact us today.
        </p>
      </div>
      <div class="col-md-6">
				<?php echo do_shortcode('[ninja_form id=5]'); ?>
        <h4 class="paragraph bold align-center text-white">
          Contact us directly
	        <?php echo (wp_is_mobile()) ? '<br/>' : ''; ?>
          <span class="text-red">214-655-2600</span>
        </h4>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
