<?php
  get_header();
  $root = get_template_directory_uri();
?>
<!-- <div class="blog-hero padding-TB-100">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1">
        <h1 class="header-title text-huge text-with-subtitle no-margin">
					<?php echo get_the_title(get_the_ID()); ?><span class="text-red">.</span>
        </h1>
      </div>
    </div>
  </div>
</div> -->
<div class="primary-content background-gray-light">
  <!-- SHAPES -->
  <div class="left-dashed-hexagon"></div>
  <div class="left-red-hexagon"></div>
  <!-- END SHAPES -->
  <div class="container-medium">
    <div class="row">
      <div class="col-sm-12">
        <?php
          if ( have_posts() ) :
            /* Start the Loop */
            while ( have_posts() ) : the_post();
            $date = get_the_date('m.d.Y');
            $category = get_the_category();
              echo '<div class="blog-metadata">';
              echo '<span class="date">'.$date.'</span>';
              echo '<a href="/category/'.$category[0]->slug.'"><span class="category">'.$category[0]->name.'</span></a>';
              echo '</div>';
              echo '<h2 class="no-margin">'. get_the_title() . '</h2>';
              the_content();
            endwhile;
            the_posts_pagination();
          else :

          endif;
        ?>
      </div>
    </div>
  </div>
  <!-- SHAPES -->
  <div class="right-dashed-hexagon"></div>
  <div class="right-red-hexagon"></div>
  <!-- END SHAPES -->
</div>

<?php get_footer(); ?>
