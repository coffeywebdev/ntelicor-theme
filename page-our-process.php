<?php
  /* Template Name: Our Process */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="ourprocess-hero background-image background-black padding-TB-100" style="background-image: url(<?php echo $root; ?>/img/ourprocess-hero-bg.jpg);">
  <!-- SHAPES -->
  <div class="left-dashed-hexagon"></div>
  <div class="left-red-hexagon"></div>
  <div class="right-dashed-hexagon"></div>
  <div class="right-red-hexagon"></div>
  <!-- END SHAPES -->
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 padding-TB-25">
        <h1 class="text-huge no-margin fade-in">Our Process<span class="text-red">.</span></h1>
        <p class="paragraph barlow subtitle no-margin text-white fade-in">Where industry-leading experience and analytics meet.</p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <p id="anim-title" class="paragraph barlow-thick text-black paragraph-big">
          Ntelicor's combination of a thorough process and an integrated AI-enhanced platform allows our experts to provide our clients with the right candidates, and our candidates with the right positions. Our strategy owes itself to three pillars<span class="text-red">:</span>
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row">
      <?php if(!wp_is_mobile()): ?>
      <div class="col-md-3">
        <img src="<?php echo $root; ?>/img/listen-heartbeat.png"/>
      </div>
      <?php endif; ?>
      <div class="col-md-7">
        <h2 class="text-big text-black text-with-subtitle no-margin">We Listen and<br/> We Learn<span class="text-red">.</span></h2>
        <p class="paragraph barlow barlow-normal text-black">
          By listening carefully, we come to understand your needs, specs, culture, and environment so that we can provide you with a shortlist of candidates who offer the skills and personality your business is seeking.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row <?php echo (wp_is_mobile()) ? '' : 'is-flex'; ?>">
      <div class="col-md-7 col-md-offset-1">
        <h2 class="text-big text-black text-with-subtitle no-margin">We Screen and <br/>Evaluate<span class="text-red">.</span></h2>
        <p class="paragraph barlow barlow-normal text-black">
          Our experienced team utilizes our consistently refined network, our unique proprietary practices, and an AI-powered integrated platform to recommend the best candidates to our clients.
        </p>
      </div>
	    <?php if(!wp_is_mobile()): ?>
      <div class="col-md-3">
        <img src="<?php echo $root; ?>/img/magnify-user.png"/>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row">
	    <?php if(!wp_is_mobile()): ?>
      <div class="col-md-3 col-md-offset-1">
        <img src="<?php echo $root; ?>/img/hands-cog.png"/>
      </div>
      <?php endif; ?>
      <div class="col-md-7">
        <h2 class="text-big text-black text-with-subtitle no-margin">We Onboard and<br/>Support<span class="text-red">.</span></h2>
        <p class="paragraph barlow barlow-normal text-black">
          Our involvement with our talent doesn't end when we hand them off to you. Our knowledgeable HR department onboards people pursuant to your requirements and our staff properly supports them for the duration of their tenure.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-100 background-black">
  <div class="container">
    <div class="row <?php echo (wp_is_mobile()) ? '' : 'is-flex'; ?>">
      <div class="col-md-4 col-md-offset-1">
        <h1 class="text-huge text-transform-none text-red">
          Work<br/>With Us<span class="text-white">.</span>
        </h1>
      </div>
      <div class="col-md-7">
        <div class="vertical-aligned-blurp">
          <p class="paragraph barlow text-white">
            Our members are highly skilled experts in their industry. We accept nothing less than the best. Joining our ranks is not about finding the next job, it's about taking the next step in an already successful IT career. Our vetting system ensures that we only recommend true IT craftsmen, not professional resume crafters.
          </p>
          <a href="/find-staff">
            <button class="button button-red-to-darkred">
              Find The Best &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
            </button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="background-image padding-TB-50" style="background:url(<?php echo $root; ?>/img/ourprocess-footer-cta.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-5">
        <h1 class="text-huge text-with-subtitle text-transform-none">
          Upgrade Your Career<span class="text-red">.</span>
        </h1>
        <p class="paragraph barlow barlow-normal text-white">
          Over 20 years Ntelicor has developed a reputation for the highest quality candidates. Our name beside a candidate serves as a seal of approval, one that tells employers you're more than a job-seeker, you're a professional. Apply today to join the ranks of our exemplary candidates.
        </p>
        <a href="/get-hired">
          <button class="button button-red-to-darkred">
            Get Hired &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
          </button>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="background-black padding-TB-50">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1 class="text-huge text-with-subtitle text-transform-none">
          Contact Us<span class="text-red">.</span>
        </h1>
        <p class="paragraph barlow text-white">
          Whether you're a candidate looking for work or a potential client looking for talent, please contact Ntelicor directly by filling out the form displayed here.
        </p>
      </div>
      <div class="col-md-6">
	      <?php echo do_shortcode('[ninja_form id=4]'); ?>
        <h4 class="text-white paragraph bold align-center">
          Contact us directly
	        <?php echo (wp_is_mobile()) ? '<br/>' : ''; ?>
          <span class="text-red">214-655-2600</span>
        </h4>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
