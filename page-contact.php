<?php
  /* Template Name: Contact Us */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="contact-hero background-image background-black" style="background-image: url(<?php echo $root; ?>/img/contact-hero-bg.jpg);">
  <div class="container-medium">
    <div class="row">
      <div class="col-md-12">
        <h1 class="text-huge text-with-subtitle fade-in">Contact Us<span class="text-red">.</span></h1>
        <p class="paragraph barlow-thin subtitle text-white fade-in">Whether you're a candidate looking for a position or a potential client looking for talent, you can contact Ntelicor directly by filling out the form below.</p>
	      <?php echo do_shortcode('[ninja_form id=4]'); ?>
        <h4 class="paragraph bold align-center text-white">
          Contact us directly
	        <?php echo (wp_is_mobile()) ? '<br/>' : ''; ?>
          <span class="text-red">214-655-2600</span>
        </h4>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
