<?php
  /* Template Name: Listings */
  get_header();
  $root = get_template_directory_uri();
?>

<div class="listings-hero background-image background-black padding-TB-100" style="background-image: url(<?php echo $root; ?>/img/listings-hero-bg.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php if(isset($_POST['job_id'])): ?>
          <h1 class="text-huge text-with-subtitle no-margin fad-in">Submission Accepted<span class="text-red">.</span></h1>
          <p class="paragraph barlow-thin subtitle text-white">Your information has been received, someone will review it and get back to you as soon as possible.</p>
        <?php else: ?>
          <h1 class="text-huge text-with-subtitle no-margin fad-in">Dream Jobs Below<span class="text-red">.</span></h1>
          <p class="paragraph barlow-thin subtitle text-white">Ntelicor knows you're not looking for just any IT job. You want one that values you, that pushes you, that allows you room to grow. That's why we only work with well established, reputable companies that meet these requirements. We want the best fit for everybody.</p>
          <p class="paragraph barlow-thin subtitle text-white">Below, you'll find our most up-to-date list of openings. For a more personalized approach, we suggest searching by job title or description. If you'd like to refer someone for a particular position, you can learn more about our Referral Program <a class="toggle-referral-program text-white bold-link" href="javascript:void(0);">here</a></p>
          <div class="referral-program">
            <h2 class="text-big text-white">Referral Program<span class="text-red">:</span></h2>
            <p class="paragraph barlow-thin subtitle text-white">Ntelicor pays a referral fee up to $1000 to those who refer a hired candidate. The fee is paid 90 days after the referred person begins work as long as that person has been working at least 90 days. The fee can vary depending on specific details and circumstances, though most referral fees are $1000. <a href="javascript:void(0);" class="button toggle-referral-program">Hide</a></p>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div class="listings-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">

          <h3 class="text-medium text-red text-bold">Search Our Job Database:</h3>
	        <?php
            // If last results were fetched more than 10 minutes ago
            if( (time() - date("U", filemtime(get_theme_file_path('/bullhorn/search_results.txt'))) > 30 )) {
              // Grab the search results table by running do_search.php and store it in search_results.txt
              ob_start();
              include get_theme_file_path('/bullhorn/do_search.php');
              $results = ob_get_clean();
              file_put_contents( get_theme_file_path('/bullhorn/search_results.txt'), $results);
            } else {
              // Otherwise use the results from search_results.html
              $results = file_get_contents( get_theme_file_path('/bullhorn/search_results.txt'));
            }

          ?>
          <!-- LISTINGS TABLE -->

            <form>
              <input id="nearby-cities" type="hidden"/>
              <div class="col-md-5 col-sm-12"><input class="listings-search" id="titles-desc" type="text" placeholder="Search Titles/Descriptions"></div>
              <div class="col-md-5 col-sm-12"><input class="listings-search" id="locations" type="text" placeholder="Search Locations"></div>
              <div class="col-md-2 col-sm-12"><input type="reset" class="listings-search-reset" id="reset" value="Reset Fields"/></div>

            </form>



          <table id="example" class="display responsive" style="width:100%">
            <thead>
            <th></th>
            <th>Job Title</th>
            <th>City</th>
            <th>State</th>
            </thead>
          </table>
          <!-- END LISTINGS TABLE -->
          <h4 class="paragraph bold align-center">
            Contact us directly
	          <?php echo (wp_is_mobile()) ? '<br/>' : ''; ?>
            <span class="text-red">214-655-2600</span>
          </h4>

      </div>
    </div>
  </div>
</div>


<?php get_template_part('template-parts/job-form'); ?>
<?php get_template_part('template-parts/submit-resume'); ?>

<?php get_footer(); ?>
