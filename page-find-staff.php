<?php
  /* Template Name: Find Staff */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="find-staff-hero background-image background-black padding-TB-100" style="background-image: url(<?php echo $root; ?>/img/ourprocess-hero-bg.jpg);">
  <!-- SHAPES -->
  <div class="left-dashed-hexagon"></div>
  <div class="left-red-hexagon"></div>
  <div class="right-dashed-hexagon"></div>
  <div class="right-red-hexagon"></div>
  <!-- END SHAPES -->
  <div class="container">
    <div class="row padding-TB-25">
      <div class="col-md-10 col-md-offset-1">
        <h1 class="text-huge text-with-subtitle no-margin fade-in">Find Staff<span class="text-red">.</span></h1>
        <p class="paragraph barlow-thin subtitle no-margin text-white fade-in">Contenders not resumes.</p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <p class="paragraph barlow barlow-normal text-black">
          We do not provide buzzword-stuffer resumes. Our network, processes, AI-powered integrated platform and experience ensure that we connect clients with candidates that fit your cultural and professional needs.
        </p>
        <p class="paragraph barlow barlow-normal text-black">
          Our centralized platform tracks all staffing activity, integrates with all AI and technology solutions to access vital information anytime, and enables consistent real-time communication.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="background-black background-image padding-TB-50" style="background-image: url(<?php echo $root; ?>/img/footer-cta-bg.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1 class="text-huge text-with-subtitle text-transform-none">
          Solutions right to your inbox<span class="text-red">.</span>
        </h1>
        <p class="paragraph barlow text-white">
          Contact us today.
        </p>
      </div>
      <div class="col-md-6">
				<?php echo do_shortcode('[ninja_form id=6]'); ?>
        <h4 class="paragraph bold align-center text-white">
          Contact us directly
	        <?php echo (wp_is_mobile()) ? '<br/>' : ''; ?>
          <span class="text-red">214-655-2600</span>
        </h4>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
