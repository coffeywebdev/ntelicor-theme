<?php
  /* Template Name: How We Help */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="how-we-help background-image padding-TB-100" style="background-image: url(<?php echo $root; ?>/img/how-we-help-hero-bg.jpg);">
  <!-- SHAPES -->
  <div class="left-dashed-hexagon"></div>
  <div class="left-red-hexagon"></div>
  <div class="right-dashed-hexagon"></div>
  <div class="right-red-hexagon"></div>
  <!-- END SHAPES -->
  <div class="container">
    <!-- ROW -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1 class="header-title text-huge text-with-subtitle no-margin fade-in">
          Premium Placement<span class="text-red">.</span>
        </h1>
        <p class="paragraph barlow-thin subtitle no-margin text-white fade-in">We'll get you to work.</p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h2 class="text-big text-black text-bold">You've got talent. <span class="text-red">Ntelicor understands your need to use it.</span></h2>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row <?php echo (wp_is_mobile()) ? '' : 'is-flex aligner-center-vertical'; ?> ">
      <div class="col-md-4">
        <h2 class="text-big text-black no-margin">We know<br/> the players<span class="text-red">.</span></h2>
      </div>
      <div class="col-md-8">
        <p class="paragraph barlow barlow-normal text-black">
          Unlike other IT staffing agencies, Ntelicor looks to find you the position and the company where you are valued as a contributor, a person, and an expert. We've been around since 1998 and have developed key relationships with leadership, management, and other team leads of a number of Fortune 50 companies.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-100">
  <div class="lg-dashed-hexagon"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2 class="text-big text-black no-margin">
          We're highly regarded in<br/>the world of IT recruiting<span class="text-red">.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <p class="paragraph barlow barlow-normal text-black">
          Our name is a stamp of approval. With our established, trusted position in the IT industry, Ntelicor has the reach and the influence to find you the position you want with a company you’ll love.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h2 class="text-big text-black no-margin">We stop the<br/>job-hop<span class="text-red">.</span></h2>
      </div>
      <div class="col-md-6">
        <p class="paragraph barlow barlow-normal text-black">
          We know that finding a new position is daunting, but finding a new position after only a few weeks at a new company can feel totally demoralizing.
          After you've been placed with Ntelicor's proprietary process and program, you'll want to stay to contribute, and to use your talents to the fullest of your ability.
          That's because we use in-depth client and job information to recommend you to an ideal position.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-100">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h2 class="text-big text-black no-margin">
          We're always available<span class="text-red">.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <p class="paragraph barlow barlow-normal text-black">
          Success is about the long-term. We make it our priority to maintain our relationship with you well into your placement. If you need us, we're there. You're always in contact with Ntelicor.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-200 background-black background-image" style="background-image: url(<?php echo $root; ?>/img/how-we-help-footer-bg.jpg);">
  <div class="container">
    <div class="col-md-9 col-md-offset-3">
      <h3 class="text-medium text-thin no-margin text-white">A Professional Recruiting Case Study:</h3>
      <h2 class="text-big text-white text-transform-uppercase no-margin">World's #1 Airline</h2>
      <div class="row">
        <div class="col-md-4">
          <h6 class="barlow text-bold text-red smaller text-with-subtitle">
            Ntelicor has more IT starts than any other IT recruitment company
          </h6>
          <p class="paragraph barlow even-smaller text-thin text-white no-margin">Past 12, 24, and 36 months</p>
        </div>
        <div class="col-md-4">
          <h6 class="barlow text-bold text-red smaller text-with-subtitle">
            More IT candidates working than any other tech staffing agency
          </h6>
          <p class="paragraph barlow even-smaller text-thin text-white no-margin">Past 12 and 24 months</p>
        </div>
        <div class="col-md-4">
          <h6 class="barlow text-bold text-red smaller">
            More candidates converted to permanent employment than any other staffing company
          </h6>
        </div>
      </div>
    </div>
    <div class="cf"></div>
  </div>
</div>
<?php get_footer(); ?>
