//var from = require('array.from');
//var assert = require('assert');

jQuery(document).ready(function($){

  var ie = msieversion();

  //console.log('ie',ie);



  // declare timeline
  var timeline1 = new TimelineMax();
  var fade_in = $('.fade-in');
  timeline1.from(fade_in,1,{x:-1000,opacity: 0, ease: Power4.easeOut});

  var slideup_fadein = $('.slideup-fadein');
  timeline1.staggerFrom(slideup_fadein,1,{y:500,opacity: 0, ease: Power4.easeOut},.1);



  //
  // // Define click action for Mobile Menu Toggle
  // $('.mobile-menu-toggle-wrapper').on('click','.mobile-menu-toggle', function(){
  //   // Calling a function in case you want to expand upon this.
  //   toggleNav();
  // });






    /* inside pages */
    // DEFINE CONTROLLER
    var page_controller = new ScrollMagic.Controller();


    var left_hexagon = $('.left-red-hexagon');
    var left_dashed_hexagon = $('.left-dashed-hexagon');

    var right_hexagon = $('.right-red-hexagon');
    var right_dashed_hexagon = $('.right-dashed-hexagon');

    var left_hexagon_timeline = new TimelineMax({repeat: -1, ease: Back.easeOut.config(2)});
    var left_dashed_hexagon_timeline = new TimelineMax({repeat: -1, ease: Back.easeOut.config(2)});

    var right_hexagon_timeline = new TimelineMax({repeat: -1, ease: Back.easeOut.config(2)});
    var right_dashed_hexagon_timeline = new TimelineMax({repeat: -1, ease: Back.easeOut.config(2)});

    left_dashed_hexagon_timeline.to(left_dashed_hexagon, 2, {y: 10});
    left_dashed_hexagon_timeline.to(left_dashed_hexagon, 2, {y: 0});
    left_dashed_hexagon_timeline.play();

    left_hexagon_timeline.to(left_hexagon, 2, {y: 12}, 1);
    left_hexagon_timeline.to(left_hexagon, 2, {y: 0});
    left_hexagon_timeline.play();

    right_dashed_hexagon_timeline.to(right_dashed_hexagon, 2, {y: 10});
    right_dashed_hexagon_timeline.to(right_dashed_hexagon, 2, {y: 0});
    right_dashed_hexagon_timeline.play();

    right_hexagon_timeline.to(right_hexagon, 2, {y: 12}, 1);
    right_hexagon_timeline.to(right_hexagon, 2, {y: 0});
    right_hexagon_timeline.play();



  /* ON SCROLL ANIMATIONS */



    var scene1 = new TimelineMax();
    scene1.to(left_dashed_hexagon, 1.5, {x: -200, ease: Power1.easeInOut})
    scene1.to(left_hexagon, 1, {x: -200, ease: Power1.easeOut}, '-=1.5');

    var scene2 = new TimelineMax();
    scene2.to(right_hexagon, 1.5, {x: 350, ease: Power1.easeInOut})
    scene2.to(right_dashed_hexagon, 1, {x: 250, ease: Power2.easeIn}, '-=1.5' )


    // Scene 1 Controllers
    new ScrollMagic.Scene({triggerHook: "onLeave", duration: "100%", triggerElement: "#page"})
      .setTween(scene1)
      //.addIndicators()
      .addTo(page_controller);

    // Scene 2 Controllers
    new ScrollMagic.Scene({
      triggerHook: "onLeave",
      duration: "100%",
      triggerElement: ".right-dashed-hexagon",
      offset: -600
    })
      .setTween(scene2)
      //.addIndicators()
      .addTo(page_controller);






}); // END DOC READY


if (typeof Object.assign != 'function') {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) { // .length of function is 2
      'use strict';
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true
  });
}

function msieversion() {

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
  {
    return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
  }
  else  // If another browser, return 0
  {
    return false;
  }
}
