jQuery(document).ready(function($) {


  $('body').on('click','.toggle-referral-program', function(){
    $('.referral-program').fadeToggle();
  });


  var table = $('#example').DataTable( {
    responsive: true,
    language: {
      zeroRecords: "Searching... Try another search if nothing turns up soon."
    },
    ajax: "/wp-content/themes/ntelicor-theme/bullhorn/search_results.txt",
    createdRow: function ( row, data, index ) {
        $(row).addClass('show-details');
    },
    columns: [
      {
       className:      'details-control',
       orderable:      false,
       sortable: false,
       data:           null,
       defaultContent: ''
      },
      {
        data: "title"
      },
      {
        data: "city"
      },
      {
        data: "state"
      },
      {
        data: "desc",
        visible: false
      },
      {
        data: "address",
        visible: false,
      }
    ],
    "order": [[1, 'asc']]
  } );



  // Add event listener for opening and closing details
  $('#example tbody').on('click', 'tr.show-details', function () {
    var tr = $(this).closest('tr');
    var row = table.row( tr );

    // TODO: hide all other rows, so that only 1 is open at a time **


    if ( row.child.isShown() ) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child( format(row.data()) ).show();
      tr.addClass('shown');
    }


  } );

  // Event listener to the two range filtering inputs to redraw on input
  $('#titles-desc, #locations').keyup( function() {
    table.draw();
  } );


  $( '#locations').keyup( function() {
    table.draw();
  $("#nearby-cities").val('');
  } );



  $('body').on('click','.listings-search-reset', function(){
    $('#titles-desc').val('');
    $('#locations').val('');
    $("#nearby-cities").val('');
    table.draw();
  });


});



/* Formatting function for row details - modify as you need */
function format ( d ) {
  // `d` is the original data object for the row
  return '<table>'+
          '<tr>'+
          '<td>'+d.desc+'</td>'+
          '</tr>'+
          '</table>';
}


jQuery.fn.dataTable.ext.search.push(
  function( settings, searchData, index, rowData, counter ) {
    var search1 = $('#titles-desc').val().toLowerCase();
    var search2 = $('#locations').val().toLowerCase();

    var title = searchData[1].toLowerCase();
    var desc = searchData[4].toLowerCase();
    var address = JSON.stringify(searchData[5]);

     // IF SEARCH1 MATCHES COLUMN 1, RETURN TRUE
    if(
      (search1=='' ) ||
      (strpos(title,search1)) ||
      (strpos(desc,search1))
    ) {
      return true;
    }


    return false;
  });



jQuery.fn.dataTable.ext.search.push(
  function( settings, searchData, index, rowData, counter ) {

    var nearby_cities = $('#nearby-cities').val();
    var search_terms = $("#locations").val().toLowerCase();
    var title = searchData[1].toLowerCase();
    var desc = searchData[4].toLowerCase();
    var address = JSON.parse(searchData[5]);
    var city = address.city.toLowerCase();
    address = toCSV(address).toLowerCase();
    search_terms = explode(',',search_terms);

    if(
      strpos(nearby_cities,city) ||
      checkEachTerm(search_terms,address) ||
      (strpos(title,search_terms[0])) ||
      (strpos(desc,search_terms[0])) ||
      search_terms == ''
    ){
      return true;
    }

    return false;
});



function checkEachTerm(search_terms,address){
  var i;
  var test;
  var st,state;
  var match,test,test2,test3,test4 = false;
  for(i=0; i < search_terms.length; i++){
    test = strpos(address,search_terms[i]);

    if(abbrState(search_terms[i],'abbr')){
      var st = abbrState(search_terms[i],'abbr').toLowerCase()
    }
    if(abbrState(search_terms[i],'name')){
      var state = abbrState(search_terms[i],'name').toLowerCase()
    }
    if(st){
      test2 = strpos(address,st);
    }
    if(state){
      test3 = strpos(address,state);
    }


    if(test>0 || test2>0 || test3 >0){
      match = true;
    }
  }
  return match;
}


var autocomplete;

function initAutocomplete() {

  var options = {
    types: ['(regions)'],
    componentRestrictions: {country: 'us'}
  };

  autocomplete = new google.maps.places.Autocomplete(document.getElementById('locations'), options);

  google.maps.event.addListener(autocomplete, 'place_changed', function () {
    $('#nearby-cities').val('')
    var place = autocomplete.getPlace();
    if(place){
      var lat = place.geometry.location.lat();
      var long = place.geometry.location.lng();
      drawTable(lat,long);
    }
  });


}




function drawTable(lat,long){

  var query_url = 'http://api.geonames.org/findNearbyPlaceNameJSON';
  var data = {lat: lat, lng: long, radius: 250, maxRows: 500, cities: 'cities5000', username: 'rockcandymedia'};
  var cities = $('#nearby-cities');

  $.ajax({
    type: "GET",
    data: data,
    url: query_url,
    dataType: 'json',
    success: function (data) {
      $.each(data.geonames, function (key, value) {
        var city = value.name;
        city = city.toLowerCase();
        cities.val(cities.val() + city + ',');
      });
      var table = $('#example').DataTable(  );
      window.setTimeout(function(){
        table.draw();
      },1)

    }
  });



}


function strpos (haystack, needle, offset) {
  var i = (haystack+'').indexOf(needle, (offset || 0));
  return i === -1 ? false : i;
}


function checkForValue(json, value) {
  for (key in json) {
    if (typeof (json[key]) === "object") {
      return checkForValue(json[key], value);
    } else if (json[key] === value) {
      return true;
    }
  }
  return false;
}


function toCSV(obj, separator) {
  var arr = [];

  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      arr.push(obj[key]);
    }
  }

  return arr.join(separator || ",");
}


function explode (delimiter, string, limit) {
  //  discuss at: http://phpjs.org/functions/explode/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //   example 1: explode(' ', 'Kevin van Zonneveld');
  //   returns 1: {0: 'Kevin', 1: 'van', 2: 'Zonneveld'}

  if (arguments.length < 2 || typeof delimiter === 'undefined' || typeof string === 'undefined') return null
  if (delimiter === '' || delimiter === false || delimiter === null) return false
  if (typeof delimiter === 'function' || typeof delimiter === 'object' || typeof string === 'function' || typeof string ===
    'object') {
    return {
      0: ''
    }
  }
  if (delimiter === true) delimiter = '1'

  // Here we go...
  delimiter += ''
  string += ''

  var s = string.split(delimiter)

  if (typeof limit === 'undefined') return s

  // Support for limit
  if (limit === 0) limit = 1

  // Positive limit
  if (limit > 0) {
    if (limit >= s.length) return s
    return s.slice(0, limit - 1)
      .concat([s.slice(limit - 1)
        .join(delimiter)
      ])
  }

  // Negative limit
  if (-limit >= s.length) return []

  s.splice(s.length + limit)
  return s
}


function abbrState(input, to){

  var states = [
    ['Arizona', 'AZ'],
    ['Alabama', 'AL'],
    ['Alaska', 'AK'],
    ['Arizona', 'AZ'],
    ['Arkansas', 'AR'],
    ['California', 'CA'],
    ['Colorado', 'CO'],
    ['Connecticut', 'CT'],
    ['Delaware', 'DE'],
    ['Florida', 'FL'],
    ['Georgia', 'GA'],
    ['Hawaii', 'HI'],
    ['Idaho', 'ID'],
    ['Illinois', 'IL'],
    ['Indiana', 'IN'],
    ['Iowa', 'IA'],
    ['Kansas', 'KS'],
    ['Kentucky', 'KY'],
    ['Kentucky', 'KY'],
    ['Louisiana', 'LA'],
    ['Maine', 'ME'],
    ['Maryland', 'MD'],
    ['Massachusetts', 'MA'],
    ['Michigan', 'MI'],
    ['Minnesota', 'MN'],
    ['Mississippi', 'MS'],
    ['Missouri', 'MO'],
    ['Montana', 'MT'],
    ['Nebraska', 'NE'],
    ['Nevada', 'NV'],
    ['New Hampshire', 'NH'],
    ['New Jersey', 'NJ'],
    ['New Mexico', 'NM'],
    ['New York', 'NY'],
    ['North Carolina', 'NC'],
    ['North Dakota', 'ND'],
    ['Ohio', 'OH'],
    ['Oklahoma', 'OK'],
    ['Oregon', 'OR'],
    ['Pennsylvania', 'PA'],
    ['Rhode Island', 'RI'],
    ['South Carolina', 'SC'],
    ['South Dakota', 'SD'],
    ['Tennessee', 'TN'],
    ['Texas', 'TX'],
    ['Utah', 'UT'],
    ['Vermont', 'VT'],
    ['Virginia', 'VA'],
    ['Washington', 'WA'],
    ['West Virginia', 'WV'],
    ['Wisconsin', 'WI'],
    ['Wyoming', 'WY'],
  ];

  if (to == 'abbr'){
    input = input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    for(i = 0; i < states.length; i++){
      if(states[i][0] == input){
        return(states[i][1]);
      }
    }
  } else if (to == 'name'){
    input = input.toUpperCase();
    for(i = 0; i < states.length; i++){
      if(states[i][1] == input){
        return(states[i][0]);
      }
    }
  }
}