jQuery(document).ready(function($){


  var ie = msieversion();
  var ie11 = detectIE();

  // console.log(ie);
  // console.log(ie11);



    // DEFINE CONTROLLER
    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onLeave", duration: "100%"}});

    // DEFINE SHAPES
    var $hero_hexagon_left = $('.shape-left');
    var $hero_hexagon_right = $('.shape-right');
    var $small_hexagon_left = $('.hexagon-2');
    var $middle_hexagon_right = $('.middle-hex-1');
    var $dashed_hex_left = $('.dashed-hexagon-no-fill,.home-shapes-wrap-2');
    var $picture = $('.picture-wrapper');

    // ANIMATIONS

    var tl = new TimelineMax({repeat: -1, ease: Back.easeOut.config(2)});
    // animate on page load
    tl.to([$hero_hexagon_left, $hero_hexagon_right], 3, {y: 15})
    tl.to([$hero_hexagon_left, $hero_hexagon_right], 3, {y: 0});
    tl.play();

    //.to($hero_hexagon_right, 12, { top: 300, ease: Back.easeInOut.config(2.7) });

    // Scene 1 Animations

    // .shape-left
    var scene1 = new TimelineMax();
    scene1.to($hero_hexagon_left, 5, {left: -300, ease: Power0.easeOut});

    // .hexagon-2
    var scene1a = new TimelineMax();
    scene1a.to($small_hexagon_left, 5, {y: -200, ease: Power0.easeNone}, 1);

    // .middle-hex-1
    var scene1b = new TimelineMax();
    scene1b.to($middle_hexagon_right, 5, {y: -150, ease: Power0.easeOut}, 0.9);  // .middle-hex-1

    var scene1c = new TimelineMax();
    scene1c.to($hero_hexagon_right, 3, {x: 350, ease: Power0.easeNone}, 1);


    // Scene 2 Animations

    // .home-shapes-wrap-2
    var scene2 = new TimelineMax();
    scene2.to($dashed_hex_left, 3, {left: -450, y: 50, ease: Back.easeOut.config(2)});

    var scene2a = new TimelineMax();
    scene2a.staggerFrom($('.client-logos img'), 1, {scale: .25,ease: Power0.easeOut}, .1);

    var scene2b = new TimelineMax();
    scene2b.to($picture,2, {right: -1000, ease: Power1.easeInOut },"-=1")


    // CONTROLLERS

    // Scene 1 Controllers
    new ScrollMagic.Scene({triggerElement: "#scene1"})
      .setTween(scene1)
      //.addIndicators()
      .addTo(controller);

    new ScrollMagic.Scene({triggerElement: "#scene1"})
      .setTween(scene1a)
      //.addIndicators()
      .addTo(controller);

    new ScrollMagic.Scene({triggerElement: "#scene1"})
      .setTween(scene1b)
      //.addIndicators()
      .addTo(controller);

    new ScrollMagic.Scene({triggerElement: "#scene1"})
      .setTween(scene1c)
      //.addIndicators()
      .addTo(controller);


    // Scene 2 Controllers
    new ScrollMagic.Scene({triggerElement: "#scene2"})
      .setTween(scene2)
      //.addIndicators()
      .addTo(controller);

    // new ScrollMagic.Scene({triggerElement: "#scene2", reverse: false, offset: 600})
    //   .setTween(scene2a)
    //   //.addIndicators()
    //   .addTo(controller);

    if(ie === false && ie11 === false) {
      new ScrollMagic.Scene({triggerElement: "#scene2"})
        .setTween(scene2b)
        //.addIndicators()
        .addTo(controller);
    }



  /* TESTIMONIALS SLIDER */

  // <i class="far fa-arrow-alt-circle-left"></i>
  // <i class="far fa-arrow-alt-circle-right"></i>
  $('#slider').flexslider({
    touch: true,
    animation: "slide",
    //smoothHeight: true,
    // Primary Controls
    controlNav: true,
    directionNav: true,
    // Secondary Navigation
    keyboard: true,
    multipleKeyboard: false,
    mousewheel: false,
    pausePlay: false,
    pauseText: 'Pause',
    playText: 'Play',
  });


  $('body').on('click','#prev-testimonial', function(){
    $('#slider').flexslider("prev") //Go to previous slide
  });

  $('body').on('click','#next-testimonial', function(){
    $('#slider').flexslider("next") //Go to next slide

  });






});



if (typeof Object.assign != 'function') {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) { // .length of function is 2
      'use strict';
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true
  });
}

function msieversion() {

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
  {
    return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
  }
  else  // If another browser, return 0
  {
    return false;
  }
}



function detectIE() {
  if(navigator.userAgent.indexOf('MSIE')!==-1
    || navigator.appVersion.indexOf('Trident/') > 0){
    return true;
  } else {
    return false;
  }
}
