$ = jQuery;
var $menu = $('.mobile-menu-wrapper');
var $menuLinks = $('#mobile-menu-links li');
var $window = $(window);

/* Animation */



// Define click action for Mobile Menu Toggle
$('.mobile-menu-toggle-wrapper').on('click','.mobile-menu-toggle', function(){
  // Calling a function in case you want to expand upon this.
  toggleNav();
});

/* while scrolling, position mobile menu depending on scroll amount */
$window.on('scroll', function(){

  var top = $window.scrollTop();
  var menuTop = $menu[0].offsetTop;
  var menuHeight = $menu.height();


    if(top > menuTop){
      //console.log('add fixed class',top,menuTop);
      $menu.addClass('fixed')

    }

    if(top < menuHeight){
      //console.log('remove fixed class',top,menuHeight);
      $menu.removeClass('fixed');

    }



});




/* Hide Mobile Menu */
function hideNav() {
  jQuery( ".mobile-menu-toggle-wrapper" ).removeClass('slidein');
  jQuery( ".mobile-menu-toggle-wrapper" ).addClass('slideout');
}

/* Show Mobile Menu */
function showNav() {
  jQuery( ".mobile-menu-toggle-wrapper" ).removeClass('slideout');
  jQuery( ".mobile-menu-toggle-wrapper" ).addClass('slidein');
}

/* toggleNav() function declaration */
function toggleNav() {
  var slideIn = new TimelineMax();
  if (jQuery('body').hasClass('show-nav')) {
    // Do things on Nav Close
    slideIn.staggerTo('.animate-in',.7,{y:-500, ease: Power2.easeOut},.05);
    slideIn.set('.animate-in', {y:0,opacity:1});

    window.setTimeout(function() {
      jQuery('body').removeClass('show-nav');
      $menuLinks.removeClass('animate-in');
    }, 700);

  } else {
    // Do things on Nav Open
    jQuery('body').addClass('show-nav');
    $menuLinks.addClass('animate-in');
    window.setTimeout(function(){  // delay animation for 300 miliseconds, after container slides down
      slideIn.staggerFrom('.animate-in',.7,{y:-500,opacity:0, ease: Power2.easeOut},.05);
    },300)
  }
}
