<?php
  /* Template Name: News Page */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="blog-hero">
<!-- SHAPES -->
<div class="left-dashed-hexagon"></div>
<div class="left-red-hexagon"></div>
<div class="right-dashed-hexagon"></div>
<div class="right-red-hexagon"></div>
<!-- END SHAPES -->
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 padding-TB-100">
				<?php
          // check if the repeater field has rows of data
          if( have_rows('hero_section') ):
            // loop through the rows of data
            while( have_rows('hero_section') ): the_row();
            $hero_headline = get_sub_field('headline');
            $hero_subheadline = get_sub_field('subheadline');
				?>
        <h1 class="header-title text-huge no-margin fade-in">
					<?php echo $hero_headline; ?><span class="text-red">.</span>
        </h1>
        <p class="paragraph barlow subtitle no-margin fade-in text-white">
					<?php echo $hero_subheadline; ?>
        </p>
        <?php
            endwhile;
          endif;
        ?>
      </div>
    </div>
  </div>

</div>
<div class="primary-content background-gray">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
      <?php get_template_part('template-parts/category-filter'); ?>
      </div>
    </div>
    <div class="row is-flex">
      <!-- START BLOG LOOP WRAPPER -->
	    <?php
		    // The Query
		    $args = array( 'post_type' => 'post' );
		    $the_query = new WP_Query( $args );
		    if ( $the_query->have_posts() ) :

			    /* Start the Loop */
			    while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
        <div class="col-md-4">
          <div class="panel blog-panel">
            <div class="random-colored-dash"></div>
            <a href="<?php echo get_the_permalink(); ?>">
              <h2 class="text-medium blog-title-archive-page"><?php echo get_the_title(); ?></h2>
            </a>
            <p class="paragraph barlow text-black">
              <?php echo the_excerpt_max_charlength(150); ?>
            </p>
            <div class="tags-wrapper">
              <p>
                <?php the_tags(); ?>
              </p>
            </div>
          </div>
        </div>
			<?php
          endwhile;
			    the_posts_pagination();
		    else :
			    echo "<h1>No posts found...</h1>";
		    endif;
	    ?>
      <!-- END BLOG LOOP WRAPPER -->
    </div>
    <!-- LOAD MORE -->
<!--    <div class="row">-->
<!--      <div class="col-lg-12">-->
<!--        <div class="align-center">-->
<!--          <a href="javascript:void(0);" class="button button-red-to-darkred">-->
<!--              Load More <i class="far fa-arrow-alt-circle-right"></i>-->
<!--          </a>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
    <!-- END LOAD MORE -->

  </div>
</div>

<?php get_footer(); ?>
