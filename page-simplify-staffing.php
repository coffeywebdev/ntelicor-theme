<?php
  /* Template Name: Simplify Staffing */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="simply-staffing background-image" style="background-image: url(<?php echo $root; ?>/img/simply-staffing-hero-bg.jpg);">
  <!-- SHAPES -->
  <div class="left-dashed-hexagon"></div>
  <div class="left-red-hexagon"></div>
  <div class="right-dashed-hexagon"></div>
  <div class="right-red-hexagon"></div>
  <!-- END SHAPES -->
  <div class="container">
    <!-- ROW -->
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1 class="header-title text-huge text-with-subtitle no-margin fade-in">
		      Simplify Staffing<span class="text-red">.</span>
        </h1>
        <p class="paragraph barlow barlow-thin subtitle no-margin text-white fade-in">
		      Make vacancies a thing of the past. Make our network your network.
        </p>
      </div>
    </div>
  </div>
</div>
<div class="padding-TB-50">
  <div class="container">
    <!-- ROW -->
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 padding-top-25">
        <h2 class="text-big text-black no-margin">How do we do it?</h2>
        <p class="paragraph barlow text-black">The Ntelicor team works tirelessly to ensure that your team can get back to business quickly. Our well-honed four-step methodology provides highly recommended professionals for all of your IT staffing needs.</p>
      </div>
    </div>
  </div>
</div>
<div class="">
  <div class="container">
    <!-- ROW -->
    <div class="row <?php echo (wp_is_mobile()) ? '' : 'is-flex'; ?> padding-TB-25">
      <div class="col-md-4">
        <h2 class="text-big text-black">
          Step 1<span class="text-red">:</span><br/>
          Thorough Professional Vetting
        </h2>
      </div>
      <div class="col-md-7">
        <div class="blurp-pushdown">
          <h3 class="text-medium text-red text-bold">
            From the many rise only the few.
          </h3>
          <p class="paragraph barlow text-black no-margin">
            Our 20+ years of experience, proprietary processes and AI-powered integrated platform provide you with a hard to access network. We screen each candidate for more than resume keywords. We take the time to understand their background and career aspirations before any recommendations are ever made. This means we look for past accomplishments and future asipirations to achieve the perfect fit for you.
          </p>
        </div>
      </div>
    </div>
    <!-- ROW -->
    <div class="row <?php echo (wp_is_mobile()) ? '' : 'is-flex'; ?> padding-TB-25">
      <div class="col-md-8">
        <h2 class="text-big text-black text-with-subtitle">
          Step 2<span class="text-red">:</span> Strong Shortlist Recommendations
        </h2>
        <h3 class="text-medium text-red text-bold">Step away from the conveyor belt of average.</h3>
        <p class="paragraph barlow text-black no-margin">
          Other staffing firms play the  numbers. They think, "If we push enough people through to our clients, we'll eventually find the right professional." Wrong.
          The assembly line methodology is costly and wastes your time. Ntelicor uses a more precise approach, one that emphasizes quality over quantity. For our clients,
          that translates into lower attrition rates and less time and money to fill vacant roles.
        </p>
      </div>
      <div class="col-md-4 position-near-bottom">
        <img src="<?php echo $root; ?>/img/sm-dashed-hexagon-no-fill.png"/>
      </div>
    </div>
    <div class="row <?php echo (wp_is_mobile()) ? '' : 'is-flex'; ?> padding-TB-25">
      <div class="col-md-3">
        <h2 class="text-big text-black">
          Step 3<span class="text-red">:</span>
          Genuine Relationship Building
        </h2>
      </div>
      <div class="col-md-6 col-md-offset-2">
        <div class="blurp-pushdown">
          <h3 class="text-medium text-red text-bold">Know exactly who you're hiring.</h3>
          <p class="paragraph barlow text-black no-margin">
            Creating the proper rapport takes time. We don't just provide a one-size-fits-all form for your future employee and send them to the waiting pool. We take the time to personally interview candidates and build a relationship. We know that the person you interview is the person we've spoken with, gotten to know, and given our full seal of approval.
          </p>
        </div>
      </div>
    </div>
    <div class="row padding-TB-50">
      <div class="col-md-8 col-md-offset-3">
        <h2 class="text-big text-black">
          Step 4<span class="text-red">:</span>
          Active and<br/> Open Communication
        </h2>
        <h3 class="text-medium text-red text-bold">Integrate with confidence. Limit the distractions.</h3>
        <p class="paragraph barlow text-black no-margin">Our job doesn't end with a successful recommendation. Ntelicor's proprietary methodology allows for simple and effective onboarding. You'll find us to be positively responsive to ensure any needs are met swiftly. </p>
      </div>
    </div>
  </div>
</div>
<div class="call-to-action background-image" style="background-image: url(<?php echo $root; ?>/img/simply-staffing-footer-bg.jpg);">
  <div class="container padding-TB-100">
    <div class="row">
      <div class="col-lg-6 col-lg-offset-6">
        <h1 class="text-huge text-transform-none">Want to know more?</h1>
        <a href="/contact-us">
          <button class="button button-red-to-darkred block-mobile">
		        Contact us with your needs &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
          </button>
        </a>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
