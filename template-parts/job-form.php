<div class="modal-overlay">
  <div id="jobForm" class="modal background-black text-white">
    <div class="modal-toggle">
      <a class="close-modal" href="javascript:void(0);"><i class="far fa-window-close"></i></a>
    </div>
    <div class="container">
        <div class="col-sm-12">
            <div class="job-form-headline"></div>
          	<form id="submit-resume-form" action="#bullhorn-messages" method="post" enctype="multipart/form-data">
          		<input type="hidden" value="" name="job_id" id="job-id-input" />
          		<input type="hidden" value="" name="job_title" id="job-title-input" />
          		<input type="hidden" value="" name="contact_id" id="contact-id-input" />
          		<div id="job-title  col-sm-12">
                <h6 class="job-title no-margin"><span id="job-title-text"></span></h6>
          		</div>
          		<input class="input2 col-sm-6" type="text" name="first_name" placeholder="First Name" required/>
          		<input class="input2 col-sm-6" type="text" name="last_name" placeholder="Last Name" required/>
          		<input id="resume-email" class="input2" type="email" name="email" placeholder="E-mail" required/>
          		<input class="input2 phone_us" type="tel" name="phone" placeholder="(xxx)xxx-xxxx" required/>
          		<div id="resume-file">
                <h2 class="job-form-headline">Submit Your Resume<span class="text-red">.</span></h2>
                <input accept=".doc,.docx" type="file" name="resume_file" id="file-1" class="inputfile inputfile-1" required/>
                <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
          		</div>
          		<textarea class="input2" placeholder="COMMENTS (optional)" name="comments"></textarea>
          		<input class="button block-mobile" id="submit-form" type="submit" value="SUBMIT NOW" />
          	</form>

        </div>

    </div>
  </div>
</div>

<!--<a href="javascript:void(0);" class="button submit-resume open-modal" data-contact="30151" data-job="5500" data-title="Job#5500 Tibco Developer (10+ Years) - Dallas">Submit Your Resume</a>-->

<script>
  jQuery(document).ready(function($){

    $('.phone_us').mask('(000) 000-0000');


    $('body').on('click', 'a.submit-resume', function(){
      // set values for hidden fields
      $('#job-id-input').val($(this).attr('data-job'));
      $('#job-title-input').val($(this).attr('data-title'));
      $('#contact-id-input').val($(this).attr('data-contact'));
      // set title in form HTML
      $('#job-title-text').html($(this).attr('data-title'));
    });

    $(document).keydown(function(event) {
      if (event.keyCode == 27) {
        $('#job-id-input').val('');
        $('#job-title-input').val('');
        $('#contact-id-input').val('');
        $('.modal-overlay').hide('fast');
      }
    });

    $('body').on('click','.open-modal', function(){
      $('.modal-overlay').show('fast');
    });

    $('body').on('click','.close-modal', function(){
      $('#job-id-input').val('');
      $('#job-title-input').val('');
      $('#contact-id-input').val('');
      $('.modal-overlay').hide('fast');
    });



  });
</script>
