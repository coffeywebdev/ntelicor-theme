<div class="col-sm-12 col-lg-8 col-lg-offset-2">
	<h1 class='text-giant text-black text-transform-none hide-small'>Uh, oh!</h1>
	<h4 class="text-red text-with-subtitle">Error Code: 404</h4>
	<h2 class="no-margin">We can't seem to find the page you are looking for<span class="text-red">.</span></h2>
	<br/>
	<div class="quick-links-wrapper">
		<h5 class="">Here are some helpful links instead:</h5>
		<div class="quick-links">
			<?php wp_nav_menu( array( 'menu' => 'Nav' ) ); ?>
		</div>
	</div>
</div>