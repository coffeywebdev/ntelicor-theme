<?php
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			?>
			<div class="col-md-4">
				<div class="panel blog-panel">
					<div class="random-colored-dash"></div>
					<a href="<?php echo get_the_permalink(); ?>">
						<h2 class="text-medium blog-title-archive-page"><?php echo get_the_title(); ?></h2>
					</a>
					<p class="paragraph barlow text-black">
						<?php echo the_excerpt_max_charlength(150); ?>
					</p>
					<div class="tags-wrapper">
						<p>
							<?php
								the_tags()
							?>
						</p>
					</div>
				</div>
			</div>
			<?php
		endwhile;
	else :
		get_template_part('template-parts/content','404');
	endif;
	// pagination
	echo "<div class='pagination-wrapper'>".the_posts_pagination()."</div>";
?>