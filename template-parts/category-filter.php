<div class="category-filters-wrapper">
	<h6 class="no-margin text-bold">Filter by: </h6>
	<ul class="category-filter">
		<li class="init bold-link filter-category">Select a category...</li>
		<?php
			foreach(get_categories() as $category):
				if($category->slug!='uncategorized')
					echo " <li data-value='/category/".$category->slug."' class='bold-link filter-category'>".$category->cat_name."</li> ";
			endforeach;
			echo " <li data-value='/news' class='bold-link filter-category'>All</li> ";
		?>
	</ul>
</div>
<script>
  jQuery(document).ready(function($){
    $("ul.category-filter").on("click", ".init", function() {
      $(this).closest("ul").children('li:not(.init)').toggle("fast");
    });

    var allOptions = $("ul.category-filter").children('.category-filter li:not(.init)');

    $("ul.category-filter").on("click", "li:not(.init)", function() {
      allOptions.removeClass('selected');
      $(this).addClass('selected');
      $("ul.category-filter").children('.init').html($(this).html());
      allOptions.toggle("fast");
      // set small timeout of 250ms, then navigate to selected category
        var category_url = $(this).data('value');
        window.setTimeout(function(){
          document.location.href = category_url;
        }, 250);
    });
  });
</script>