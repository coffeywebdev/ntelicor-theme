<?php

	if(isset($_POST['job_id'])) {

	//  include 'config.php';
	$email_subject = 'Applicant for Position #' . $_POST['job_id'];

	$ntelicor_emails = array();
	$ntelicor_emails['1'] = 'diana.cooper@ntelicor.com.test-google-a.com';
	$ntelicor_emails['2'] = 'deanna.rivera@ntelicor.com.test-google-a.com';
	$ntelicor_emails['3'] = 'mark.cohen@ntelicor.com.test-google-a.com';
	$ntelicor_emails['2146'] = 'jordan.ramsey@ntelicor.com.test-google-a.comm';
	$ntelicor_emails['27871'] = 'aaron.jenkins@ntelicor.com.test-google-a.com';
	$ntelicor_emails['5772'] = 'angela.andrews@ntelicor.com.test-google-a.com';
	$ntelicor_emails['13047'] = 'julien.jeantet@ntelicor.com.test-google-a.com';
	$ntelicor_emails['22386'] = 'diana.reyes.moeck@ntelicor.com.test-google-a.com';
	$ntelicor_emails['30748'] = 'sarah.baroody@ntelicor.com.test-google-a.com';
	$ntelicor_emails['24797'] = 'christy.kimball@ntelicor.com.test-google-a.comm';
	$ntelicor_emails['24798'] = 'jennifer.nelson@ntelicor.com.test-google-a.com';
	$ntelicor_emails['30151'] = 'judy.orosco@ntelicor.com.test-google-a.com';
	$ntelicor_emails['34013'] = 'caitlin.studley@ntelicor.com.test-google-a.com';
	$ntelicor_emails['38970'] = 'isaac.feldhaus@ntelicor.com.test-google-a.com';
	$ntelicor_emails['46492'] = 'sean.corbray@ntelicor.com.test-google-a.com';
	$ntelicor_emails['50453'] = 'mike.bonolis@ntelicor.com.test-google-a.com';

	$fallback_email = 'deanna.rivera@ntelicor.com.test-google-a.com';

  $error = false;
  $success = false;


    if ($_FILES["resume_file"]["error"] > 0) {
      $error = "File Upload Error: " . $_FILES["resume_file"]["error"] . "<br />Please try submitting your resume again.";
    } else {
      $basefile = time() . "_" . $_FILES["resume_file"]["name"];
      $current_dir = getcwd();
      $filename = $current_dir . "/resumes/" . $basefile;
      $domain = $_SERVER['SERVER_NAME'];
      //$fileurl = "http://www.ntelicor.com/resumes/" . $basefile;
      $fileurl = $domain . "/resumes/" . $basefile;

      move_uploaded_file($_FILES["resume_file"]["tmp_name"], $filename);
      

      $contact_found = array_key_exists($_POST['contact_id'], $ntelicor_emails);

      ob_start();


	 if(!$contact_found) { ?>
		<h3>No Match for Job Contact Info ID!</h3>
		Please contact HypeLife Brands to notify us what email address Job Orders assigned to contact ID <?php echo($_POST['contact_id']); ?> should be sent to.
		<br />
		<br />
		<?php } ?>

		<strong>Job Title: </strong> <?php echo($_POST['job_title']); ?><br />
		<strong>Job ID: </strong> <?php echo($_POST['job_id']); ?><br />
		<br />
		<strong>Name: </strong> <?php echo($_POST['first_name'] . ' ' . $_POST['last_name']); ?><br />
		<strong>Email: </strong> <a href="mailto:<?php echo($_POST['email']); ?>"><?php echo($_POST['email']); ?></a><br />
		<strong>Phone: </strong> <?php echo($_POST['phone']); ?><br />
		<strong>Resume: </strong> <a href="<?php echo($fileurl); ?>"><?php echo($basefile); ?></a><br />

		<?php if(strlen($_POST['comments']) > 4) { ?>
			<strong>Comments: </strong>
			<br />
			<?php echo(stripslashes(trim($_POST['comments'])));
		}


      $mail_body = ob_get_clean();

      // Match $_POST['contact_id'] to an email address from above
      $to = $contact_found ? $ntelicor_emails[$_POST['contact_id']] : $fallback_email;
      // $to = "coffey.j.anthony@gmail.com";

      $header .= "Reply-To: ". $_POST['first_name'] . ' ' . $_POST['last_name'] . " <" . $_POST['email'] . ">\r\n";
      $header .= "Return-Path: ". $to ."\r\n";
      $header .= "Return-Receipt-To: ". $to ."\r\n";
      $header .= 'MIME-Version: 1.0' . "\n";
      $header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

      wp_mail($to, $email_subject, $mail_body, $header);


      $success = true;
    }
  }

?>