<?php
  /* Template Name: Home Page */
  get_header();
  $root = get_template_directory_uri();
?>
<div class="hero-video-wrapper">
  <video autoplay="" loop="" id="video-background" muted="" plays-inline="">
    <source src="<?php echo $root; ?>/assets/header-video.mp4" type="video/mp4">
  </video>
</div>
<div id="scene1" class="hero-section">

  <!-- SHAPES -->
  <div class="home-shapes-wrap">
    <div class="shape-left"></div>
    <div class="shape-right"></div>

  </div>
  <div class="home-shapes-wrap-2">
    <div class="dashed-hexagon-no-fill"></div>

  </div>
  <!-- END SHAPES -->
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1">
				<?php
					$hero_heading = get_field('hero_headline');
					$hero_subheading = get_field('hero_subheadline');
				?>
        <h1 class="header-title text-huge text-with-subtitle fade-in"><div class="dash"></div>
					<?php echo $hero_heading; ?><span class="text-red">.</span>
        </h1>
        <h2 class="text-white text-medium header-subtitle fade-in">
					<?php echo $hero_subheading; ?>
        </h2>
      </div>
    </div>
  </div>
  <div class="container">
    <div id="hero-panels-wrapper" class="row <?php echo (wp_is_mobile()) ? '' : 'is-flex'; ?>">
			<?php
				// check if the repeater field has rows of data
				if( have_rows('hero_panels') ):

					// loop through the rows of data
					while( have_rows('hero_panels') ): the_row();
						// display a sub field value
						$panel_heading = get_sub_field('panel_headline');
						$panel_copy = get_sub_field('panel_copy',false);
						$panel_link = get_sub_field('button_link');
						$panel_button = get_sub_field('button_text');
						?>
            <div class="col-lg-5 col-lg-push-1 col-md-5 col-md-push-1">
              <div class="panel box-shadow-sm slideup-fadein">
                <h2 class="text-medium align-center"><?php echo $panel_heading; ?><span class="text-red">.</span></h2>
                <p class="align-center panel-paragraph"><?php echo $panel_copy; ?></p>
                <p class="align-center">
                  <a class="button button-red-to-darkred block-mobile" href="<?php echo $panel_link;  ?>" title="<?php echo $panel_button; ?>">
										<?php echo $panel_button; ?> &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
                  </a>
                </p>
              </div>
            </div>
						<?php

					endwhile;

				endif;

			?>
    </div>
  </div>
</div>
<?php

	if( have_rows('section_1') ):

	while( have_rows('section_1') ): the_row();

	$section1_heading = get_sub_field('headline');
	$section1_copy = get_sub_field('copy',false);
	$section1_link = get_sub_field('button_link');
	$section1_button_text = get_sub_field('button_text');
	$section1_bgimg = get_sub_field('background_image');
?>
<div id="scene2" class="section-1" style="">
  <!-- SHAPES -->
  <!--<div class="dashed-hexagon-wrapper">
    <div class="dashed-hexagon"></div>
  </div>-->
  <!-- END SHAPES -->
  <div class="container">
    <div id="fill-positions-content-wrapper" class="row" style="">
      <div class="col-lg-4 col-lg-offset-2 col-md-offset-5 col-md-offset-1">
        <div class="picture-wrapper">
          <div class="picture" style="background-image: url(<?php echo $section1_bgimg; ?>);"></div>
        </div>
        <div class="hexagons-wrapper">
          <div class="hexagon-2"></div>
          <div class="hexagon-1"></div>
        </div>
        <div class="fill-positions-blurp">
          <h2 class="text-big text-transform-none text-with-subtitle text-white"><?php echo $section1_heading; ?></h2>
          <p class="paragraph max-width barlow text-white"> <?php echo $section1_copy; ?></p>
            <a title="<?php echo $section1_button_text; ?>" href="<?php echo $section1_link; ?>" class="button button-red-to-darkred block-mobile">
            <?php echo $section1_button_text; ?> &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
            </a>
        </div>
      </div>
    </div>
  </div>
</div>
		<?php
	endwhile;
	endif;
?>
<div class="our-clients section-2">
  <div class="container">
    <div class="row">
	    <?php
		    if( have_rows('section_2') ):

		    // loop through the rows of data
		    while ( have_rows('section_2') ) : the_row();
		    $section2_headline = get_sub_field('headline');
		    $section2_copy = get_sub_field('copy',false);
	    ?>
      <div class="col-sm-12 col-lg-4">
        <h1 class="text-huge text-black small-line-height text-with-subtitle no-top-margin"><div class="dash-red"></div><?php echo $section2_headline; ?></h1>
        <p class="paragraph barlow text-black">
		      <?php echo $section2_copy; ?>
        </p>
      </div>
      <div class="col-sm-12 col-lg-8">
        <div class="row client-logos-wrapper">
	      <?php
          //$first_multiple = (ntelicor_is_mobile()) ? 2 : 3;
          //$second_multiple = (ntelicor_is_mobile()) ? 4 : 6;
		      if( have_rows( 'client_logos') ):
			      //$count = 1;
			      while( have_rows( 'client_logos') ): the_row();
				      $img_src = get_sub_field('logo_image');
				      ?>

                <div class="client-logos col-md-4 col-sm-12 col-xs-12"><img src="<?php echo $img_src; ?>"/></div>

				      <?php /*if($count == $first_multiple || $count == $second_multiple): ?>
                <div class="cf"></div>
				      <?php endif;*/ ?>

				      <?php
				      //$count++;
			      endwhile;
		      endif;
		    ?>
        </div>
      </div>
		    <?php endwhile; ?>
		    <?php endif; ?>
    </div>
  </div>
  <div class="home-shapes-wrap-3">
    <div class="hexagon-3"></div>
    <div class="cf"></div>
  </div>
</div>
<div class="background-black it section-3">
  <div class="container">
    <div class="row">
      <div class="col-md-3 aligner-center-vertical it-img">
        <img alt="Find the best IT Staffing for your company!" src="<?php echo $root; ?>/img/IT.gif" loop="infinite">
      </div>
      <div class="col-md-9">
        <?php
          if( have_rows('section_3') ):
            while( have_rows('section_3') ): the_row();
              $section3_headline = get_sub_field('headline');
              $section3_copy = get_sub_field('copy',false);
              $section3_button_link = get_sub_field('button_link');
              $section3_button_text = get_sub_field('button_text');
        ?>
              <h2 class="text-big text-red text-with-subtitle">
                <?php echo $section3_headline; ?><span class="text-white">.</span>
              </h2>
              <p class="paragraph text-white barlow">
                <?php echo $section3_copy; ?>
                <br/>
                <a title="<?php echo $section3_button_text; ?>" href="/find-staff" class="button button-red-to-darkred block-mobile no-border">
                <?php echo $section3_button_text; ?> &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
                </a>
              </p>
            <?php endwhile; ?>
          <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div class="section-4">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-lg-offset-6">
        <?php
          if( have_rows('section_4') ):
            while( have_rows('section_4') ): the_row();
              $section4_headline = get_sub_field('headline');
              $section4_copy = get_sub_field('copy', false);
              $section4_button_link = get_sub_field('button_link');
              $section4_button_text = get_sub_field('button_text');
        ?>
        <h2 class="text-big text-red text-with-subtitle">
          <?php echo $section4_headline; ?><span class="text-white">.</span>
        </h2>
        <p class="paragraph text-white barlow">
          <?php echo $section4_copy; ?>
          <br/>
          <br/>
          <a title="<?php echo $section4_button_text; ?>" class="button button-red-to-darkred block-mobile no-border" href="<?php echo $section4_button_link; ?>">
            <?php echo $section4_button_text; ?> &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
          </a>
        </p>
        <?php
            endwhile;
          endif;
        ?>
      </div>
    </div>
  </div>
</div>
<div class="section-5 background-black">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-lg-7 animated-usa-wrapper">
        <video autoplay="" loop="" id="animated-usa" muted="" plays-inline="">
          <source src="<?php echo $root; ?>/assets/animated-usa.mp4" type="video/mp4">
        </video>
      </div>
      <div class="col-sm-12 col-lg-5 animated-usa-copy">
        <?php
          if( have_rows('section_5') ):
            while( have_rows('section_5') ): the_row();
              $section5_headline = get_sub_field('headline');
              $section5_copy = get_sub_field('copy', false);
              $section5_button_link = get_sub_field('button_link');
              $section5_button_text = get_sub_field('button_text');
        ?>
        <h2 class="text-big text-red text-with-subtitle <?php echo (wp_is_mobile()) ? 'padding-LR-30' : ''; ?>">
          <?php echo $section5_headline; ?><span class="text-white">.</span>
        </h2>
        <p class="paragraph text-white barlow">
          <?php echo $section5_copy; ?>
          <br/>
          <br/>
          <a title="<?php echo $section5_button_text; ?>" class="button button-red-to-darkred block-mobile no-border" href="<?php echo $section5_button_link; ?>">
            <?php echo $section5_button_text; ?> &nbsp; <i class="far fa-arrow-alt-circle-right"></i>
          </a>
        </p>
        <?php
            endwhile;
          endif;
        ?>
      </div>
    </div>
  </div>

</div>
<div class="testimonials section-6 background-black">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="text-huge text-white text-transform-none text-with-subtitle"><div class="dash-red"></div>Testimonials</h1>
      </div>
    </div>
      <div class="testimonial-slider">
        <!-- TESTIMONIAL SLIDER -->
        <div class="row">
          <div class="col-sm-12">
            <!-- LEFT QUOTE -->
            <div class="left-quote-wrapper">
              <div class="left-quote"></div>
            </div>
            <div id="slider">
              <ul class="slides">
              <?php if( have_rows('testimonials', 'option') ): ?>
                  <?php while( have_rows('testimonials', 'option') ): the_row(); ?>
                    <?php
                      $review = get_sub_field('review');
                      $author = get_sub_field('authors_name');
                      $author_title = get_sub_field('authors_title');
                    ?>
                  <li>
                    <p class="barlow text-white"><?php echo $review; ?></p>
                    <p class="text-white author-title no-margin">- <?php echo $author_title; ?></p>
                    <p class="text-white author-name no-margin"><?php echo $author; ?></p>
                  </li>
                  <?php endwhile; ?>
              <?php endif; ?>
              </ul>
            </div>
            <!-- RIGHT QUOTE -->
            <div class="right-quote-wrapper">
              <div class="right-quote"></div>
            </div>
            <!-- PAGINATION -->
            <div class="slider-buttons">
              <a title="Previous Testimonial" id="prev-testimonial" href="javascript:void(0);" class="button"><i class="far fa-arrow-alt-circle-left"></i></a>
              <a title="Next Testimonial" id="next-testimonial" href="javascript:void(0);" class="button"><i class="far fa-arrow-alt-circle-right"></i></a>
            </div>

          </div>
        </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
