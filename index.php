<?php
	get_header();
	$root = get_template_directory_uri();
?>
<div class="primary-content background-gray padding-LR-15">
  <!-- SHAPES -->
  <div class="left-dashed-hexagon"></div>
  <div class="left-red-hexagon"></div>
  <div class="right-dashed-hexagon"></div>
  <div class="right-red-hexagon"></div>
  <!-- END SHAPES -->
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
	      <?php get_template_part('template-parts/category-filter'); ?>
      </div>
    </div>
    <div class="row">
      <?php get_template_part('template-parts/content-loop'); ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>
