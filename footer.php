<?php $root = get_template_directory_uri(); ?>
<?php
	$topbar_red_button1_text = get_field('top_bar_red_button_1_text', 'option');
	$topbar_red_button1_link = get_field('top_bar_red_button_1_link', 'option');
	$topbar_red_button2_text = get_field('top_bar_red_button_2_text', 'option');
	$topbar_red_button2_link = get_field('top_bar_red_button_2_link', 'option');
?>
<?php if(is_page('news')): ?>
<div class="footer-cta background-black">
  <div class="container">
    <div class="col-lg-5">
      <h1 class="text-huge text-transform-none text-with-subtitle">Solutions right to<br/> your inbox<span class="text-red">.</span></h1>
      <p class="paragraph barlow text-white subtitle">Contact us today.</p>
    </div>
    <div class="col-lg-6 col-lg-offset-1 padding-TB-25">
      <?php echo do_shortcode('[ninja_form id=4]'); ?>
      <h4 class="paragraph bold align-center text-white">
        Contact us directly
	      <?php echo (wp_is_mobile()) ? '<br/>' : ''; ?>
        <span class="text-red">214-655-2600</span>
      </h4>
    </div>
  </div>
</div>
<?php endif; ?>
<footer class="background-black">
	<div class="container">
    <div class="row">
      <div class="col-sm-12">
        <nav class="main-menu hide-small">
          <?php wp_nav_menu( array( 'theme_location' => 'nav-menu' ) ); ?>
        </nav>
      </div>
    </div>
		<div class="row">
			<div class="col-sm-12 col-md-4 logo align-center aligner-center-vertical">
				<a href="/"><img src="<?php echo $root; ?>/img/ntelicor-logo.svg" alt="Ntelicor logo" /></a>
			</div>
      <div class="col-sm-12 col-md-8">
        <ul class="social-links">
          <li>
            <a href="https://www.facebook.com/ntelicor.co/" title="Follow Us on Facebook">Facebook</a>
          </li>
          <li><a href="https://www.instagram.com/ntelicorusa/" title="Follow Us on Instagram">Instagram</a></li>
          <li><a href="https://www.linkedin.com/company/ntelicor-l-p-/" title="Follow Us on LinkedIn">LinkedIn</a></li>
          <li><a href="https://twitter.com/ntelicor" title="Follow Us on Twitter">Twitter</a></li>
        </ul>
      </div>
		</div>

		<div class="row">
      <div class="col-sm-12">
			  <p class="paragraph bold text-white align-center small-letter-spacing">Powered by Rock Candy Media</p>
      </div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
<?php if(is_page('get-hired')): ?>
<script type="text/javascript">
  jQuery(document).ready(function($){
    $('body').on('click','button.btn.btn-success.nf-fu-fileinput-button', function(event){
      event.preventDefault();
    });
  });
</script>
<?php endif; ?>
</html>
