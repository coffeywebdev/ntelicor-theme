<?php


/* Add Menu Support */
add_theme_support( 'menus' );

/* Add Thumbnail Support */
add_theme_support('post-thumbnails');


/* Hide Admin Bar */
//add_filter('show_admin_bar', '__return_false');

/* Register Menus */
function register_my_menu() {
  register_nav_menu('menu-mobile',__( 'Menu Mobile' ));
  register_nav_menu('nav-menu',__( 'Nav Menu' ));
}
add_action( 'init', 'register_my_menu' );

/* Register Options Page */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_page('Testimonials');
}

/* REMOVE STUFF FOR LIGHTER LOAD */
remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'feed_links', 2 );
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0 );
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action('wp_head', 'wp_generator');
remove_action('wp_head','jetpack_og_tags');
//remove_action('wp_footer','jquery');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );




	function ntelicor_scripts() {
		// include font awesome
		//wp_enqueue_style( 'ntelicor-fontawesome', '', array(), null );
		// include minified stylesheet
		//wp_enqueue_style( 'ntelicor-style', get_template_directory_uri().'css/sierra.css',array(),null );

		wp_enqueue_script( 'jquery');
		wp_enqueue_script( 'jquery-ui');
		wp_enqueue_script('jquery-migrate', '//code.jquery.com/jquery-migrate-1.2.1.min.js"',null,null,true);

		wp_enqueue_script('custom-file-upload-field', get_theme_file_uri('/js/jquery.custom-file-input.js'), array('jquery'), null, true);
		wp_enqueue_script('jquery-input-mask', get_theme_file_uri('/js/jquery.mask.min.js'), array('jquery'), null, true);

		// data tables!
		wp_enqueue_style('datatables-style', '//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css');
		wp_enqueue_script('datatables-script', '//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',null,null,true);

		wp_enqueue_script( 'flex-slider',get_theme_file_uri('/js/jquery.flexslider-min.js'),array('jquery'),null,true);

		//wp_enqueue_script( 'smooth-scroll', get_theme_file_uri( '/js/smooth-scroll.min.js'),null,null,true);

		/* GREENSOCK */
		wp_enqueue_script('tweenmax', "https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js",null,null,true);
		//wp_enqueue_script('gsap-jquery','https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/jquery.gsap.min.js',null,null,true);
		//wp_enqueue_script('gsap-css','https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/plugins/CSSRulePlugin.min.js','',null,true);

		wp_enqueue_script('scroll-magic', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js',null,null,true);
		wp_enqueue_script('scroll-magic-gsap', 'http://scrollmagic.io/scrollmagic/uncompressed/plugins/animation.gsap.js',null,null,true);
		wp_enqueue_script('scroll-magic-indicators', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js',null,null,true);




		wp_enqueue_script('mainmenu-js', get_theme_file_uri('/js/menu.js'),array('jquery'),null,true);




		if(get_the_ID() == 2){
			wp_enqueue_script('homepage-animations-js', get_theme_file_uri('/js/home.js'),array('jquery'),null,true);
		} else {
			wp_enqueue_script('app-js', get_theme_file_uri('/js/app.js'),array('jquery'),null,true);
		}

		if(get_the_ID() == 124):
		wp_enqueue_script('ourprocess-js', get_theme_file_uri('/js/ourprocess.js'),array('jquery'),null,true);
		endif;

		if(get_the_ID() == 131):
			wp_enqueue_script('cookie-js', get_theme_file_uri('/js/js.cookie.js'), array('jquery'), null, true);
			wp_enqueue_script('listings-js', get_theme_file_uri('/js/listings.js'),array('jquery'),null,true);
			wp_localize_script('listings-js', 'localized', array('gmaps_api' => 'AIzaSyACn0DmfW_YodtETknyeuOcOaEc1YQpDp0') );
			wp_enqueue_script('gmaps-api','https://maps.googleapis.com/maps/api/js?key=AIzaSyACn0DmfW_YodtETknyeuOcOaEc1YQpDp0&libraries=places&callback=initAutocomplete',null,null, true);
			//
		endif;
	}
	add_action( 'wp_enqueue_scripts', 'ntelicor_scripts' );

/* custom excerpt length */


function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '[...]';
	} else {
		echo $excerpt;
	}
}


function ntelicor_is_mobile() {
	static $is_mobile;

	if ( isset($is_mobile) )
		return $is_mobile;

	if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
		$is_mobile = false;
	} elseif (
		strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
		|| strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
		|| strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
		|| strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
		|| strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
		$is_mobile = true;
	} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
		$is_mobile = true;
	} elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
		$is_mobile = false;
	} else {
		$is_mobile = false;
	}

	return $is_mobile;
}


function ntelicor_sender_email( $original_email_address ) {
	return 'website@ntelicor.com';
}

// Function to change sender name
function ntelicor_sender_name( $original_email_from ) {
	return 'Ntelicor.com';
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'ntelicor_sender_email' );
add_filter( 'wp_mail_from_name', 'ntelicor_sender_name' );




/* query jobs with cron job */

add_action( 'get_jobs',  'ntelicor_query_jobs' );
// Add function to register get_jobs to WordPress init
add_action( 'init', 'register_get_jobs_cronjob');

// Function which will register the event
function register_get_jobs_cronjob() {
	// Make sure this event hasn't been scheduled
	if( !wp_next_scheduled( 'get_jobs' ) ) {
		// Schedule the event
		wp_schedule_event( time(), 'daily', 'get_jobs' );
	}
}



function ntelicor_query_jobs(){

/* ****************** */
// Create a new instance of the SoapClient class.
	$params = array('trace' => 1, 'soap_version' => SOAP_1_1);
	$BHclient = new SoapClient("https://api.bullhornstaffing.com/webservices-1.1/?wsdl",$params);

	// To run this code, you will need a valid username, password, and API key.
	$username = "Ntelicor.api";
	$password = "490lisbon";
	$apiKey = "C2EB8E6E-E164-B256-C80139FC60B82EEE";

	//old password 490lisbon

	// Start a new session
	$session_request = new stdClass();
	$session_request->username = $username;
	$session_request->password = $password;
	$session_request->apiKey = $apiKey;
	$API_session = $BHclient->startSession($session_request);
	$API_currentSession = $API_session->return;

	// Create an array with the query parameters
	$query_array = array(
		'entityName' => 'JobOrder',
		'maxResults' => 1000,
		//'where' => 'isDeleted=0',
		//'where' => 'isOpen=1',
		//'where' => "customText12 = 'Yes'",
		//'where' => "customText12 = 'Yes' AND dateAdded >= '07/01/2015'",
		'where' => "customText12 = 'Yes' AND isOpen=1 AND isDeleted=0",
		'orderBys' => array('dateAdded desc'),
		'parameters' => array()
	);

	// Create the DTO type that the API will understand by casting the array to the dtoQuery
	// type that the query operation expects.
	$SOAP_query = new SoapVar($query_array, SOAP_ENC_OBJECT,"dtoQuery", "http://query.apiservice.bullhorn.com/");

	// Put the DTO into a request object
	$request_array = array ('session' => $API_currentSession, 'query' => $SOAP_query);

	// Cast the request as a query type
	$SOAP_request = new SoapVar($request_array, SOAP_ENC_OBJECT, "query", "http://query.apiservice.bullhorn.com/");

	// Use the query method to return the candidate ids
	try {
		$queryResult = $BHclient->query($SOAP_request);
	} catch (SoapFault $fault) {
		var_dump($BHclient->__getLastRequest());
		die($fault->faultstring);
	}


	// Use the find() method to retrieve the candidate DTO for each Id
	// Loop through each Id in the query result list
	$i = 0;
	$data = array();

	foreach ($queryResult->return->ids as $value) {
		$i++;
		// Cast each Id to an integer type
		$findId = new SoapVar($value, XSD_INTEGER,"int","http://www.w3.org/2001/XMLSchema");

		// Create the find() method request
		$find_request = array(
			'session' => $API_currentSession,
			'entityName' => 'JobOrder',
			'id' => $findId
		);

		// Use the find() method to return the candidate dto
		try {
			$findResult = $BHclient->find($find_request);
		} catch (SoapFault $fault) {
			var_dump($BHclient->__getLastRequest());
			die($fault->faultstring);
		}

		$listing_object = $findResult->return->dto;
		$address = $listing_object->address;
		$state = convertStateAbbrv($address->state);
		$city = $address->city;
		$title = $findResult->return->dto->title;  // append city, state to job listing title
		$explode_title = explode('#',$title);
		$job_title_cleaned = substr($explode_title[1],5);
		$title = $job_title_cleaned;
		$desc = $findResult->return->dto->publicDescription;
		$contact = $findResult->return->dto->responseUserID;
		$job = $value;
		$apply_link = "<a class='button submit-resume open-modal' data-contact='".$findResult->return->dto->responseUserID."' data-job='".$value."' data-title='".$findResult->return->dto->title."'>Submit Your Resume</a>";
		$desc .= $apply_link;
		$address->state = $state;
		$data['data'][] = array('title'=>$title,'desc'=>$desc, 'address'=>json_encode($address), 'city'=>$city, 'state'=>$state);

	}

	$results = json_encode($data);
	file_put_contents( get_theme_file_path('/bullhorn/search_results.txt'), $results);

}

/* HELPER FUCTION TO CONVERT STATE ABBRV. TO STATE NAME */

function convertStateAbbrv($abbrv){
	$states = array(
		'AL'=>'ALABAMA',
		'AK'=>'ALASKA',
		'AS'=>'AMERICAN SAMOA',
		'AZ'=>'ARIZONA',
		'AR'=>'ARKANSAS',
		'CA'=>'CALIFORNIA',
		'CO'=>'COLORADO',
		'CT'=>'CONNECTICUT',
		'DE'=>'DELAWARE',
		'DC'=>'DISTRICT OF COLUMBIA',
		'FM'=>'FEDERATED STATES OF MICRONESIA',
		'FL'=>'FLORIDA',
		'GA'=>'GEORGIA',
		'GU'=>'GUAM GU',
		'HI'=>'HAWAII',
		'ID'=>'IDAHO',
		'IL'=>'ILLINOIS',
		'IN'=>'INDIANA',
		'IA'=>'IOWA',
		'KS'=>'KANSAS',
		'KY'=>'KENTUCKY',
		'LA'=>'LOUISIANA',
		'ME'=>'MAINE',
		'MH'=>'MARSHALL ISLANDS',
		'MD'=>'MARYLAND',
		'MA'=>'MASSACHUSETTS',
		'MI'=>'MICHIGAN',
		'MN'=>'MINNESOTA',
		'MS'=>'MISSISSIPPI',
		'MO'=>'MISSOURI',
		'MT'=>'MONTANA',
		'NE'=>'NEBRASKA',
		'NV'=>'NEVADA',
		'NH'=>'NEW HAMPSHIRE',
		'NJ'=>'NEW JERSEY',
		'NM'=>'NEW MEXICO',
		'NY'=>'NEW YORK',
		'NC'=>'NORTH CAROLINA',
		'ND'=>'NORTH DAKOTA',
		'MP'=>'NORTHERN MARIANA ISLANDS',
		'OH'=>'OHIO',
		'OK'=>'OKLAHOMA',
		'OR'=>'OREGON',
		'PW'=>'PALAU',
		'PA'=>'PENNSYLVANIA',
		'PR'=>'PUERTO RICO',
		'RI'=>'RHODE ISLAND',
		'SC'=>'SOUTH CAROLINA',
		'SD'=>'SOUTH DAKOTA',
		'TN'=>'TENNESSEE',
		'TX'=>'TEXAS',
		'UT'=>'UTAH',
		'VT'=>'VERMONT',
		'VI'=>'VIRGIN ISLANDS',
		'VA'=>'VIRGINIA',
		'WA'=>'WASHINGTON',
		'WV'=>'WEST VIRGINIA',
		'WI'=>'WISCONSIN',
		'WY'=>'WYOMING',
		'AE'=>'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST',
		'AA'=>'ARMED FORCES AMERICA (EXCEPT CANADA)',
		'AP'=>'ARMED FORCES PACIFIC'
	);
	return ucwords(strtolower($states[$abbrv]));
}


?>